﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Ball01 : MonoBehaviour
{
    public float zforce = 20;
    public float speed;
    public Camera maincamera;
    public Transform direction;
    public Transform arrow;
    public Transform Flag;
    public Rigidbody Ball;
    public float arrowzscale = 0.07f;
    public Canvas scoreboard;
    public Text hole_num, par_num, stroke_num, scoreboard_stroke_num1, scoreboard_stroke_num2, scoreboard_stroke_num3;
    int par, stroke; 
    static int hole1_score, hole2_score, hole3_score = 0;

    void Start()
    {
        scoreboard.enabled = false;
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Course01") 
        {
            hole_num.text = "#1";
            par_num.text = "2";
            par = 2;
        }
        if (currentScene.name == "Course02")
        {
            hole_num.text = "#2";
            par_num.text = "5";
            par = 5;
        }
        if (currentScene.name == "Course03")
        {
            hole_num.text = "#3";
            par_num.text = "4";
            par = 4;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown("space")) 
        {
            scoreboard.enabled = !scoreboard.enabled;
            scoreboard_stroke_num1.text = hole1_score.ToString();
            scoreboard_stroke_num2.text = hole2_score.ToString();
            scoreboard_stroke_num3.text = hole3_score.ToString();
        }

        if(Input.GetKeyDown("w")) 
        {
            if(zforce > 375)
            {
                zforce = 375;
                arrowzscale = 0.14f;
                arrow.GetComponent<Transform>().localScale = new Vector3(.02f,.01f,arrowzscale);
            }
            else
            {
                zforce += 50;
                arrowzscale += .01f;
                arrow.GetComponent<Transform>().localScale = new Vector3(.02f,.01f,arrowzscale);
            }
        }
        if(Input.GetKeyDown("s")) 
        {
            if(zforce < 25)
            {
                zforce = 25;
                arrowzscale = 0.07f;
                arrow.GetComponent<Transform>().localScale = new Vector3(.02f,.01f,arrowzscale);
            }
            else
            {
                zforce -= 50;
                arrowzscale -= .01f;
                arrow.GetComponent<Transform>().localScale = new Vector3(.02f,.01f,arrowzscale);
            }
        }
        if(Input.GetKey("a")) Ball.GetComponent<Transform>().transform.Rotate(0,-1,0);
        if(Input.GetKey("d")) Ball.GetComponent<Transform>().transform.Rotate(0,1,0);
        maincamera.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
        stroke_num.text = stroke.ToString();
        if(stroke > par) stroke_num.color = Color.red;
    }

    void OnMouseDown()
    {
        if(GetComponent<Rigidbody>().velocity == Vector3.zero)
        {
            Ball.GetComponent<Rigidbody>().AddRelativeForce(0,0,zforce);
            arrow.GetComponent<MeshRenderer>().enabled = false;
            StartCoroutine(StopTheBall());
            stroke += 1;
            Scene currentScene = SceneManager.GetActiveScene();
            if (currentScene.name == "Course01") 
            {
                hole1_score += 1;
            }
            if (currentScene.name == "Course02")
            {
                hole2_score += 1;
            }
            if (currentScene.name == "Course03")
            {
                hole3_score += 1;
            }
        }
        else return;
        
    }

    void OnTriggerEnter(Collider obj)
    {
        if(obj.name == "hole01trigger")
        {
            if(stroke > par) scoreboard_stroke_num1.color = Color.red;
            scoreboard_stroke_num1.text = hole1_score.ToString();
            scoreboard.enabled = true;
        } 
        if(obj.name == "hole02trigger")
        {
            if(stroke > par) scoreboard_stroke_num2.color = Color.red;
            scoreboard_stroke_num2.text = hole2_score.ToString();
            scoreboard.enabled = true; 
        }
        if(obj.name == "hole03trigger")
        {
            if(stroke > par) scoreboard_stroke_num3.color = Color.red;
            scoreboard_stroke_num3.text = hole3_score.ToString();
            scoreboard.enabled = true; 
        }
        if(obj.name == "Environment01") SceneManager.LoadScene("Course01");//error handling UI screen, reload hole on "ok" button click
        if(obj.name == "Environment02") SceneManager.LoadScene("Course02");//error handling UI screen, reload hole on "ok" button click
        if(obj.name == "Environment03") SceneManager.LoadScene("Course03");//error handling UI screen, reload hole on "ok" button click
    }

    public void Continue()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Course01") 
        {
            SceneManager.LoadScene("Course02"); 
        }
        if (currentScene.name == "Course02") 
        {
            SceneManager.LoadScene("Course03");
        }
        if (currentScene.name == "Course03") 
        {
            SceneManager.LoadScene("Menu");
        }
    }
    

    IEnumerator StopTheBall()
    {
        Debug.Log("hello");
        yield return new WaitForSeconds(1);
        yield return new WaitUntil(() => GetComponent<Rigidbody>().velocity == Vector3.zero);
        Ball.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        yield return new WaitUntil(()=>GetComponent<Rigidbody>().velocity==Vector3.zero);
        Ball.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        arrow.GetComponent<Transform>().localScale = new Vector3(.02f,.01f,.07f);
        Ball.GetComponent<Rigidbody>().rotation = Quaternion.Euler(0,0,0);
        Vector3 targetPostition = new Vector3(Flag.position.x,this.transform.position.y,Flag.position.z);
        this.transform.LookAt(targetPostition);
        zforce = 50;
        arrow.GetComponent<MeshRenderer>().enabled = true;
    }
}
 